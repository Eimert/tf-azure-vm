# main.tf
# Author 		: Eimert Vink

#                     #
#    Variables        #
#                     #

variable "name" {
  description = "Project name."
  default     = "FinchExtreme"
}
variable "resource_group" {
  description = "The resource group to create the resources under."
  default     = "vms"
}
variable "location" {
  description = "What datacenter location?"
  default     = "West Europe"
}
variable "storage_account_name" {
  description = "Unique storage account name (lowercase 3 - 24 chars)"
  default     = "frenchgarden"
}
variable "storage_container_name" {
  description = "storage container name"
  default     = "major"
}
variable "vm_size" {
  description = "The Azure vm_size. https://azure.microsoft.com/en-us/pricing/details/virtual-machines/series/"
  default     = "Standard_F4" # Standard_F4
}
variable "env" {
  description = "The environment identifier"
  default     = "staging"
}



#                                   #
#      Foundational resources       #
#                                   #

# resource group
resource "azurerm_resource_group" "resource_group" {
  name     = "${var.resource_group}"
  location = "${var.location}"
}

# storage account
resource "azurerm_storage_account" "storage_account" {
  name                = "${var.storage_account_name}" # must be unique on Azure
  resource_group_name = "${azurerm_resource_group.resource_group.name}"

  location            = "${var.location}"
  account_type        = "Standard_LRS"

  tags {
    environment       = "${var.env}"
  }
}

# storage container
resource "azurerm_storage_container" "storage_container" {
  name                  = "${var.storage_container_name}"
  resource_group_name   = "${azurerm_resource_group.resource_group.name}"
  storage_account_name  = "${azurerm_storage_account.storage_account.name}"
  container_access_type = "private"
  depends_on            = ["azurerm_storage_account.storage_account"]
}

# virtual network
resource "azurerm_virtual_network" "virtual_network" {
  name                = "${var.env}TestVirtualNetwork1"
  address_space       = ["10.0.0.0/16"]
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.resource_group.name}"
}

# subnet
resource "azurerm_subnet" "subnet" {
  name                 = "${var.env}Subnet"
  virtual_network_name = "${azurerm_virtual_network.virtual_network.name}"
  resource_group_name  = "${azurerm_resource_group.resource_group.name}"
  address_prefix       = "10.0.2.0/24"
}







module "server1" {
  source                   = "./vm"


  name                     = "${var.name}"
  hostname                 = "puppy"
  vm_size                  = "${var.vm_size}"
  username                 = "testadmin"
  password                 = "NottinghamHill123"

  sa_blob_endpoint         = "${azurerm_storage_account.storage_account.primary_blob_endpoint}"
  storage_container_name   = "${var.storage_container_name}"
  subnet                   = "${azurerm_subnet.subnet.id}"



  resource_group           = "${var.resource_group}"
  location                 = "${var.location}"
  env                      = "${var.env}"

}