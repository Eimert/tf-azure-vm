# vm.tf
# Author    : Eimert Vink

# Other values
variable "name" {}
variable "hostname" {}
variable "vm_size" {}
variable "sa_blob_endpoint" {}
variable "storage_container_name" {}
variable "subnet" {}
variable "username" {}
variable "password" {}


variable "resource_group" {}
variable "location" {}
variable "env" {}



# public ip
resource "azurerm_public_ip" "pub_ip" {
  name                            = "${var.name}-${format("publicip-%03d", count.index+1)}"
  location                        = "${var.location}"
  resource_group_name             = "${var.resource_group}"
  public_ip_address_allocation    = "static"
  domain_name_label               = "${var.hostname}"

  tags {
    environment                   = "${var.env}"
  }
}

# network interface
resource "azurerm_network_interface" "iface" {
  name                            = "${var.name}-${format("iface-%03d", count.index+1)}"
  location                        = "${var.location}"
  resource_group_name             = "${var.resource_group}"

  ip_configuration {
    name                          = "ip_config"
    subnet_id                     = "${var.subnet}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.pub_ip.id}"
  }
}

# virtual machine
resource "azurerm_virtual_machine" "vm" {
  name                            = "${var.hostname}"
  location                        = "${var.location}"
  resource_group_name             = "${var.resource_group}"
  network_interface_ids           = ["${azurerm_network_interface.iface.id}"]
  vm_size                         = "${var.vm_size}"

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.3"
    version   = "latest"
  }

  storage_os_disk {
    name          = "${var.hostname}disk"
    vhd_uri       = "${var.sa_blob_endpoint}${var.storage_container_name}/${var.hostname}disk.vhd"
    caching       = "ReadWrite"
    create_option = "FromImage"
  }
  delete_os_disk_on_termination   = true

  os_profile {
    computer_name  = "${var.hostname}"
    admin_username = "${var.username}"
    admin_password = "${var.password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  tags {
    environment = "${var.env}"
  }
}

# provisioner