# Terraform Azure VM
Create multiple CentOS VM's on Azure. Terraform code partly in module layout, making it easy to reproduce VM's.
## Module layout
- The 'vm' module has all the configuration for a single vm.
You can duplicate the 'vm' module and create a heavy vm (buildserver) by altering the vm_size. Or choose to use UbuntuServer instead of CentOS in storage_image_reference.

## Usage
Quick guide to spinning up these VM's.
### Azure credentials
Open a shell and export your azure credentials. [Get credentials from Azure](http://toddysm.com/2016/12/08/how-to-configure-terraform-to-work-with-azure-resource-manager/).
```
export ARM_SUBSCRIPTION_ID=
export ARM_CLIENT_ID=
export ARM_CLIENT_SECRET=
export ARM_TENANT_ID=
```
### Adapt
Alter the default username/password of the vm in main.tf. The module 'vm' can be duplicated to easily create multiple VM's.

## Creating the infrastructure
Navigate to the root directory of this terraform module.
```
cd <terraform dir>
terraform get
terraform plan
terraform apply
terraform destroy
```

## Accessing the infrastructure

```
ssh testadmin@<hostname>.<location>.cloudapp.azure.com
```
Login using the credentials defined in the vm module.